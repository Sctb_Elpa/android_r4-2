package ru.sctbelpa.r4_2.halpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.coefficient.FloatCoefficient;
import ru.sctbelpa.r4_2.coefficient.IntCoefficient;
import android.util.Log;

import com.wordpress.codesuxxx.xml.SimpleXPath;
import com.wordpress.codesuxxx.xml.SimpleXPath.XNode;
import com.wordpress.codesuxxx.xml.XNodeException;

public class CoefficientsXMLFile extends File implements CoefficientFileInterface{

	private static final String DeviceCellsChangeTransaction = "DeviceCellsChangeTransaction";
	private static final String DevIDElement = "DevID";
	private static final String DevIDval = "56077";
	private static final String FileVersionElement = "FileVersion";
	private static final String FileVersionval = "2";
	private static final String TransactionCellsList = "TransactionCellsList";
	private static final String CellData = "CellData";
	private static final String cellAddress = "cellAddress";
	private static final String CellType = "CellType";
	private static final String HoldingRegister = "HoldingRegister";
	private static final String Name = "Name";
	private static final String T_mp = "Время измерения давления";
	private static final String T_mt = "Время измерения температуры";
	private static final String AsyncValue = "AsyncValue";
	private static final String coeff_str = "Коэффициент";
	private static final String Address = "Address";
	private static final String cellTypeString = "cellTypeString";
	private static final String Type_U16 = "System.UInt16";
	private static final String Type_Float = "System.Single";
	private static final String A_coeff = "A";
	private static final String root_elementName = "root";
	private static final String _h = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?><" + root_elementName + ">\n";
	private static final String _end = "</" + root_elementName + ">\n";
	private static final String _ins = "[INSERT_THERE]";

	private static final int A0_COEFF_ADDR = 7;

	private static final Pattern coeffnamePattern = 
			Pattern.compile(coeff_str + " " + "(.*)([0-9])");

	public static final String LOG_TAG = "Xml parser";

	private final List<Coefficient> coeffsList;

	public CoefficientsXMLFile(String path, List<Coefficient> coeffsList) {
		super(path);
		this.coeffsList = coeffsList;
	}

	private void validete(XNode node) throws ParseException
	{
		try {
			if (node.getStringAttribute(DevIDElement).equals(DevIDval) &&
					node.getStringAttribute(FileVersionElement).equals(FileVersionval))
				return;
		} catch (XNodeException e) {}
		throw new ParseException(
				R4_2App.getAppContext().getResources().getString(
						R.string.invalid_input_file_format), 
						0);
	}

	private Coefficient parceCoefficient(XNode cell_info)
	{
		try {
			XNode celltype = cell_info.getChild(cellAddress + "/" + CellType);
			if (celltype.getValue().equals(HoldingRegister))
			{

				XNode name = cell_info.getChild(Name);
				String name_str = name.getValue();
				String value = cell_info.getChild(AsyncValue).getValue();
				if (name_str.equals(T_mp)) {
					return new IntCoefficient(
							R.string.T_mp, 
							value, 
							R.string.ms);
				} else if (name_str.equals(T_mt)) {
					return new IntCoefficient(
							R.string.T_mt, 
							value, 
							R.string.ms);
				} else { 
					Matcher m = coeffnamePattern.matcher(name_str);
					if (m.matches())
					{
						String coeffname;
						if (m.group(2).equals("0") && !m.group(1).equals(A_coeff))
							coeffname = m.group(1) + m.group(2);
						else
						{
							if (m.group(1).equals(A_coeff))
								// A[0]-> A[1]
								coeffname = m.group(1) + "[" + 
										(Integer.parseInt(m.group(2)) + 1) + "]";
							else
								coeffname = m.group(1) + "[" + m.group(2) + "]";
						}
						return new FloatCoefficient(coeffname, Float.parseFloat(value));
					}
				}
			}
		} catch (XNodeException e) {
			return null;
		}
		return null;
	}

	@Override
	public void Import() throws IOException, ParseException {
		Log.i(LOG_TAG, "Importing coeffs from " + this.getName());

		SimpleXPath simpleXPath;
		try {
			simpleXPath = new SimpleXPath(
					new FileInputStream(this));
		} catch (ParserConfigurationException | SAXException e) {
			throw new ParseException(e.getLocalizedMessage(), 0);
		}

		XNode node = simpleXPath.getXPathNode(simpleXPath.getRoot(), 
				DeviceCellsChangeTransaction);

		validete(node);

		try {
			for (XNode cell_info : node.getChildren(TransactionCellsList + "/" + CellData))
			{
				Coefficient coeff = parceCoefficient(cell_info);;
				if (coeff != null)
					// replace
					for (Coefficient item : coeffsList)
						if (item.getName().equals(coeff.getName()))
							item.setValueObj(coeff.valueObj());
			}
		} catch (XNodeException e) {
			throw new ParseException(
					R4_2App.getAppContext().getResources().getString(
							R.string.invalid_input_file_format), 
							0);
		}
	}

	@Override
	public void Export() throws IOException {
		Log.i(LOG_TAG, "Exporting coeffs to " + this.getName());

		// bulding XML
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return;
		}
		Document document = documentBuilder.newDocument();
		Element root = document.createElement(root_elementName);
		document.appendChild(root);

		for (int i = 0; i < coeffsList.size(); ++i)
		{
			Coefficient c = coeffsList.get(i);
			Element e = serialiseCoefficient(
					document, c);
			root.appendChild(e);
		}

		// output
		DOMSource domSource = new DOMSource(document);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(domSource, result);
		} catch (TransformerException  e) {
			throw new IOException(e.getMessage());
		}
		StringBuilder editor = new StringBuilder(writer.toString());
		int index = editor.indexOf(_h);
		editor.delete(index, index + _h.length());
		index = editor.indexOf(_end);
		editor.delete(index, index + _end.length());

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
				R4_2App.getAppContext().getResources().openRawResource(R.raw.template),
				"UTF-8"));

		FileOutputStream fwriter = new FileOutputStream(this);
		while(true)
		{
			String tmp = bufferedReader.readLine();
			if (tmp == null)
				break;

			if (tmp.trim().equals(_ins))
				fwriter.write(editor.toString().getBytes());
			else
			{
				fwriter.write(tmp.getBytes());
				fwriter.write('\n');
			}
		}
		fwriter.close();
	}

	private Element serialiseCoefficient(Document d, Coefficient c) {
		Element result = d.createElement(CellData);

		Element cellAddressElement = d.createElement(cellAddress);

		Element AddressElement = d.createElement(Address);
		AddressElement.appendChild(d.createTextNode(String.valueOf(c.getAddress())));

		Element CellTypeElement = d.createElement(CellType);
		CellTypeElement.appendChild(d.createTextNode(HoldingRegister));

		cellAddressElement.appendChild(AddressElement);
		cellAddressElement.appendChild(CellTypeElement);

		result.appendChild(cellAddressElement);

		String typeStr;
		Element cellTypeStringElement =  d.createElement(cellTypeString);
		switch (c.getType())
		{
		case Coefficient.TYPE_INT:
			cellTypeStringElement.appendChild(d.createTextNode(Type_U16));
			typeStr = "xsd:unsignedShort";
			break;
		case Coefficient.TYPE_FLOAT:
			cellTypeStringElement.appendChild(d.createTextNode(Type_Float));
			typeStr = "xsd:float";
			break;
		default:
			typeStr = "";
			cellAddressElement.appendChild(d.createTextNode(""));
		}
		result.appendChild(cellTypeStringElement);

		Element NameElement =  d.createElement(Name);
		String coeffName = c.getName();
		if (coeffName.equals(
				R4_2App.getAppContext().getResources().getString(R.string.T_mp)))
			NameElement.appendChild(d.createTextNode(T_mp));
		else if (coeffName.equals(
				R4_2App.getAppContext().getResources().getString(R.string.T_mt)))
			NameElement.appendChild(d.createTextNode(T_mt));
		else {
			Pattern p = Pattern.compile("(.)\\[([0-9])\\]");
			Matcher m = p.matcher(coeffName);
			if (m.matches())
			{
				if (m.group(1).equals(A_coeff))
					NameElement.appendChild(d.createTextNode(
							coeff_str + " " + m.group(1) +
							(Integer.parseInt(m.group(2)) - 1)));
				else
					NameElement.appendChild(d.createTextNode(
							coeff_str + " " + m.group(1) +
							m.group(2)));
			}
			else
				NameElement.appendChild(d.createTextNode(
						coeff_str + " " + coeffName));
		}
		result.appendChild(NameElement);

		Element vlaueElement = d.createElement(AsyncValue);
		vlaueElement.setAttribute("xsi:type", typeStr);
		vlaueElement.appendChild(d.createTextNode(c.toString()));

		result.appendChild(vlaueElement);

		return result;
	}

}
