package ru.sctbelpa.r4_2.halpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ScreenReceiver extends BroadcastReceiver {

    private ScreeenListener listener = null;
    
    public interface ScreeenListener {
    	public void OnScreenStatusChanged(boolean isOn);
    }
    
    public ScreenReceiver() {}
    
    public void setOnScreenStatusChanged(ScreeenListener listener)
    {
    	this.listener = listener;
    }
 
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // do whatever you need to do here
            if (listener != null)
            	listener.OnScreenStatusChanged(false);            
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // and do whatever you need to do here
        	if (listener != null)
            	listener.OnScreenStatusChanged(false);
        }
    }
}
