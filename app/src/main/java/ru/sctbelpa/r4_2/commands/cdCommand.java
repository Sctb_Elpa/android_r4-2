package ru.sctbelpa.r4_2.commands;

import ru.sctbelpa.r4_2.FileListActivity;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class cdCommand extends Command {

	private final String destPath;
	private String resultPath;

	public cdCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket, String path) {
		super(h, callbackRessiver, socket);

		destPath = path;
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof FileListActivity)
					((FileListActivity) callbackRessiver).RereadDirectory();
			}
		});
	}

	@Override
	public String command() {
		return "cd " + destPath;
	}

	@Override
	protected void onFail(int code) {
		super.onFail(code);

		if (code == ERR_PARCE)
		{
			h.post(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(callbackRessiver.getContext(), 
							R4_2App.getAppContext().getResources().getString(R.string.Failed_to_open_dir) +
							" " + destPath, Toast.LENGTH_LONG).show();;
				}
			});
		}
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.startsWith("/"))
		{
			resultPath = ansver.trim();
			onSuccess();
		}
		else 
			onFail(ERR_PARCE);
	}

}
