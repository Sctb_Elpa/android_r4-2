package ru.sctbelpa.r4_2.commands;

import java.text.SimpleDateFormat;
import java.util.Date;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class SyncDateCommand extends Command {

	public static final String SET_DATE_FORMAT =
			"MMddHHmmyyyy.ss"; 
	
	public SyncDateCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
	}

	@Override
	protected void onSuccess() {
	}

	@Override
	public String command() {
		SimpleDateFormat formatter = new SimpleDateFormat(SET_DATE_FORMAT);
		return "date " + formatter.format(new Date());
	}

	@Override
	protected void onFail(int code) {
		super.onFail(code);
		h.post(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(R4_2App.getAppContext(), 
						R.string.date_update_fail, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.contains("Date updated"))
			onSuccess();
		else
			onFail(0xff);
	}
}
