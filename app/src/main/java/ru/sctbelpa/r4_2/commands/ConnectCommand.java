package ru.sctbelpa.r4_2.commands;

import ru.sctbelpa.r4_2.FileListActivity;
import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.halpers.Pinger;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;


public class ConnectCommand extends Command {

	public ConnectCommand(Handler h, myActivtyInterface ressiver,
			BluetoothSocket socket) {
		super(h, ressiver, socket);
	}

	@Override
	protected void onFail(int code) {
		super.onFail(code);

		h.post(new Runnable() {

			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)	
					((MainActivity)callbackRessiver).RefrashStatus();
				
				Toast.makeText(callbackRessiver.getContext(),
						R.string.Connect_failed, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {}

	@Override
	protected void onSuccess() {
		
		Pinger.StartPinger();
		
		h.post(new Runnable() {

			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)				
					((MainActivity)callbackRessiver).RefrashStatus();
				else if (callbackRessiver instanceof FileListActivity)
					((FileListActivity) callbackRessiver).RereadDirectory();
			}
		});
	}
}
