package ru.sctbelpa.r4_2.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

public class CurrentCommand extends Command {

	private static final String pattern = 
			"([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*);([0-9A-Fa-f]*)"; 
	private final Pattern p;
	
	private float res[];

	public CurrentCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
		
		p = Pattern.compile(pattern);
		res = new float[4];
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)				
					((MainActivity)callbackRessiver).updateValues(res);
			}
		});
	}
	
	@Override
	protected void parseAnsver(String ansver) {

		Matcher m = p.matcher(ansver);
		if (m.find())
		{
			for (int i = 0; i < 4; ++i)
				res[i] = hex2float(m.group(i + 1));
			
			onSuccess();
		}
	}

	@Override
	public String command() {
		return "current";
	}
}
