package ru.sctbelpa.r4_2.commands;

import java.io.IOException;

import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.halpers.Pinger;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class DisconnectCommand extends Command {

	public DisconnectCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
	}

	@Override
	protected void onFail(int code) {
		System.exit(0);
	}

	@Override
	protected void onSuccess() {
		
		R4_2App.RecreateBtSocket();
		
		h.post(new Runnable() {

			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)				
					((MainActivity)callbackRessiver).RefrashStatus();
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {}

	@Override
	public void run() {
		Pinger.StopPinger();
		
		if (socket.isConnected())
		{
			try {
				socket.close();
			} catch (IOException e) {
				Log.e(LOG_TAG, "Fatal. Can\'t close socket");
				onFail(0);
			}
			Log.i(LOG_TAG, "Disconnected.");
			onSuccess();
		}
	}
}
