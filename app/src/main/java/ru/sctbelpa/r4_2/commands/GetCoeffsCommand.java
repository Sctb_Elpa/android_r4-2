package ru.sctbelpa.r4_2.commands;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.coefficient.FloatCoefficient;
import ru.sctbelpa.r4_2.coefficient.IntCoefficient;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class GetCoeffsCommand extends Command {

	private static final int ERR_PARCE = 0xff;

	private static final Pattern coeffPattern = Pattern.compile("([0-9A-Fa-f]*)");

    private int coeffsAddrs_6[] = {7, 9, 0xb, 0xd, 0xf, 0x11, 0x13, 0x15,
            0x17, 0x19, 0x1b, 0x1d, 0x1f,
            3, 4, 0x21};
    private int coeffsAddrs_16[] = {7, 9, 0xb, 0xd, 0xf, 0x11, 0x13, 0x15,
            0x17, 0x19, 0x1b, 0x1d, 0x1f, 0x21, 0x23, 0x25, 0x27, 0x29, 0x2B, 0x2D, 0x2F, 0x31,
            0x33, 3, 4, 0x35};

	private final List<Coefficient> result = new ArrayList<>();

	public GetCoeffsCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)
					((MainActivity)callbackRessiver).showCoeffsEditDialog(result);
			}
		});
	}

	private List<String> extract_coeffs(String bundle) {
	    List<String> result = new ArrayList<>();

	    Matcher m = coeffPattern.matcher(bundle);
	    while (m.find()) {
            final String group = m.group(1);
            if (!group.isEmpty())
                result.add(group);
        }
        return result;
    }

	@Override
	protected void parseAnsver(String ansver) {
        List<String> parced = extract_coeffs(ansver);
        int coeffsAddrs[];
        int P_coeffs_count;

        switch (parced.size()) {
            case 26:
                P_coeffs_count = 16;
                coeffsAddrs = coeffsAddrs_16;
                break;
            case 16:
                P_coeffs_count = 6;
                coeffsAddrs = coeffsAddrs_6;
                break;
            default:
                onFail(ERR_PARCE);
                return;
        }

        Coefficient c;
        int i = 0;
        for (; i < P_coeffs_count; ++i) {
            c = new FloatCoefficient(String.format("A[%d]", i), parced.get(i));
            c.setAddress(coeffsAddrs[i]);
            result.add(c);
        }

        c = new FloatCoefficient("Fp0", parced.get(i));
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        c = new FloatCoefficient("Ft0", parced.get(i));
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        c = new FloatCoefficient("T0", parced.get(i));
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        for (int j = 1; j < 4; ++j) {
            c = new FloatCoefficient(String.format("C[%d]", j), parced.get(i));
            c.setAddress(coeffsAddrs[i++]);
            result.add(c);
        }

        c = new FloatCoefficient("F0", parced.get(i));
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        c = new IntCoefficient(R.string.T_mp, parced.get(i), R.string.ms);
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        c = new IntCoefficient(R.string.T_mt, parced.get(i), R.string.ms);
        c.setAddress(coeffsAddrs[i++]);
        result.add(c);

        c = new IntCoefficient(R.string.mU, parced.get(i), "");
        c.setAddress(coeffsAddrs[i]);
        result.add(c);

        onSuccess();
	}

	@Override
	public String command() {
		return "coeffs";
	}

	@Override
	protected void onFail(int code) {
		if (code != ERR_CONNECT)
		{
			h.post(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(
							R4_2App.getAppContext(), R.string.readCoeffs_fail, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
		else
			super.onFail(code);
	}
}
