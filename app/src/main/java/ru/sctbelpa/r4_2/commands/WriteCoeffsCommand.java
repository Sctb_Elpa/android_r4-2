package ru.sctbelpa.r4_2.commands;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

import java.util.Formatter;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;

public class WriteCoeffsCommand extends Command {

	private static final int ERR_IN_RESSIVER = 0xff;

	private final String CoeffsStr;

	public WriteCoeffsCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket, List<Coefficient> coeffs) {
		super(h, callbackRessiver, socket);
		StringBuilder sb = new StringBuilder();

		final Iterator<Coefficient> it = coeffs.iterator();
		while (it.hasNext()) {
			sb.append(it.next().formatOutput());
			if (it.hasNext())
				sb.append(';');
		}

		CoeffsStr = sb.toString();
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(R4_2App.getAppContext(), 
						R.string.write_coeffs_success, Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {
		if (ansver.contains("[Accepted]"))
			onSuccess();
		else
			onFail(ERR_IN_RESSIVER);
	}

	@Override
	public String command() {
		return "coeffs " + CoeffsStr;
	}

	@Override
	protected void onFail(int code) {
		if (code != ERR_CONNECT)
			h.post(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(R4_2App.getAppContext(), 
							R.string.write_coeffs_failed, Toast.LENGTH_LONG).show();
				}
			});
		else
			super.onFail(code);
	}

}
