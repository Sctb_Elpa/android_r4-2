package ru.sctbelpa.r4_2.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public abstract class Command implements Runnable {

	public static final int ERR_CONNECT = 0;
	public static final int ERR_RESSIVE = 1;
	public static final int ERR_NOANSVER = 2;
	public static final int ERR_SEND = 3;
	public static final int ERR_PARCE = 0xf0;
	
	public static final String LOG_TAG = "Command";
	protected final Handler h;
	protected final myActivtyInterface callbackRessiver;
	protected final BluetoothSocket socket;
	
	protected long timeoutMS = 150;
	protected long after_1st_timeout = 2 * timeoutMS / 3;

	public Command(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		this.h = h;
		this.callbackRessiver = callbackRessiver;
		this.socket = socket;
	}
	
	public String command()
	{
		return "";
	}
	
	protected void onFail(int code)
	{
		switch (code) {
		case ERR_CONNECT:
			R4_2App.RecreateBtSocket();
			break;
		case ERR_PARCE:
			Log.e(LOG_TAG, "Incorrect Ansver");
			break;
		}
	}
	
	protected abstract void onSuccess();
	protected abstract void parseAnsver(String ansver);

	@Override
	public void run() {
		
		InputStream In;
		OutputStream Out;
		
		// prepare
		try {
			if (!socket.isConnected())
			{
				socket.connect();
				Log.i(LOG_TAG, "Connected");
			}
			
			In = socket.getInputStream();
			Out = socket.getOutputStream(); 
		} catch (IOException e) {
			Log.e(LOG_TAG, "Failed to connect: " + e.getMessage());
			onFail(ERR_CONNECT);
			return;
		}
				
		if (command() == null || command().isEmpty())
		{
			onSuccess();
			return;
		}
		
		// send command
		try {
			Out.write((command() + "\n\r").getBytes());
		} catch (IOException e1) {
			onFail(ERR_SEND);
			return;
		}
				
		String ansver = new String();
				
		int avalable;
		
		long d = System.currentTimeMillis();
		long cTimeout = timeoutMS;
		
		while(!Thread.interrupted() &&
				((d + cTimeout) >= System.currentTimeMillis()))
		{
			try {
				if ((avalable = In.available()) > 0)
				{
					byte[] b = new byte[avalable];
					In.read(b);
					ansver += new String(b);
					d = System.currentTimeMillis(); // reset timeout
					cTimeout = after_1st_timeout;
				}
				else
					Thread.sleep(1);
			} catch (IOException e) {
				Log.e(LOG_TAG, "Read error!");
				onFail(ERR_RESSIVE);
				return;
			} catch (InterruptedException e) {
				return;
			}
		}
		if (!ansver.isEmpty())
			parseAnsver(ansver);
		else
		{
			Log.e(LOG_TAG, "No Ansver Ressived!");
			onFail(ERR_NOANSVER);
		}
		
		// flush
		try {
			if ((avalable = In.available()) > 0)
			{
				byte[] b = new byte[avalable];
				In.read(b);
			}
		} catch (IOException e) {}
	}
	
	public static float hex2float(String hexStr)
	{
		Long i = Long.parseLong(hexStr, 16);
        return Float.intBitsToFloat(i.intValue());
	}
}
