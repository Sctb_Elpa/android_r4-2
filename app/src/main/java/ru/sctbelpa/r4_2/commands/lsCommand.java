package ru.sctbelpa.r4_2.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.r4_2.FileListActivity;
import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.R4_2App;
import ru.sctbelpa.r4_2.FsModel.FSContent.FsObject;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.widget.Toast;

public class lsCommand extends Command {
	
	static final Pattern openFailedPattern = 
			Pattern.compile("Open (.*) failed \\((.*)\\)");

	private final String path;
	private List<FsObject> result; 

	public lsCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket, String path) {
		super(h, callbackRessiver, socket);

		this.path = path;
		
		timeoutMS = 300;
	}

	@Override
	public String command() {
		return "ls " + path;
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof FileListActivity)
					((FileListActivity) callbackRessiver).lsResult(result);
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {

		Matcher m = openFailedPattern.matcher(ansver);
		
		if (m.matches())
		{
			Toast.makeText(callbackRessiver.getContext(), 
					R4_2App.getAppContext().getResources().getString(
							R.string.Failed_to_open_remote) + 
							m.group(1) + " (" + m.group(2) + ")",
					Toast.LENGTH_LONG).show();
			
			onFail(ERR_PARCE);
		}
		else {
			result = new LinkedList<FsObject>();
			for (String fname : ansver.split("\n"))
				if (!fname.equals("\r") && !fname.isEmpty())
					result.add(new FsObject(fname.trim()));

			onSuccess();
		}
	}

}
