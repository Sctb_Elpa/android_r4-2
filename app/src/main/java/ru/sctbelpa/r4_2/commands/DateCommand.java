package ru.sctbelpa.r4_2.commands;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.sctbelpa.r4_2.MainActivity;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class DateCommand extends Command {

	// Now: 30.05.2015 15:17:34
	public static final String dateFormat = 
			"dd.MM.yyyy HH:mm:ss";
	private Date result;

	public DateCommand(Handler h, myActivtyInterface callbackRessiver,
			BluetoothSocket socket) {
		super(h, callbackRessiver, socket);	
	}

	@Override
	protected void onSuccess() {
		h.post(new Runnable() {
			
			@Override
			public void run() {
				if (callbackRessiver instanceof MainActivity)				
					((MainActivity)callbackRessiver).updateDeviceClockDisplay(result);
			}
		});
	}

	@Override
	protected void parseAnsver(String ansver) {

		if (ansver.startsWith("Now: "))
		{
			String t = ansver.replaceFirst("Now: ", "");
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			try {
				result = formatter.parse(t);
			} catch (ParseException e) {
				// invalid format
				Log.w(LOG_TAG, "incorrect date ansver: " + ansver);
				onFail(0);
				return;
			}
			onSuccess();
		}
	}

	@Override
	public String command() {
		return "date";
	}
}
