package ru.sctbelpa.r4_2;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.commands.Command;
import ru.sctbelpa.r4_2.commands.ConnectCommand;
import ru.sctbelpa.r4_2.commands.CurrentCommand;
import ru.sctbelpa.r4_2.commands.DateCommand;
import ru.sctbelpa.r4_2.commands.DisconnectCommand;
import ru.sctbelpa.r4_2.commands.GetCoeffsCommand;
import ru.sctbelpa.r4_2.commands.PingCommand;
import ru.sctbelpa.r4_2.commands.SyncDateCommand;
import ru.sctbelpa.r4_2.commands.WriteCoeffsCommand;
import ru.sctbelpa.r4_2.commands.catCommand;
import ru.sctbelpa.r4_2.commands.cdCommand;
import ru.sctbelpa.r4_2.commands.lsCommand;
import ru.sctbelpa.r4_2.halpers.ScreenReceiver;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

public class R4_2App extends Application {

	public static final String LOG_TAG = "R4_2App";

	public static final int CMD_LS = 0;
	public static final int CMD_PWD = 1;
	public static final int CMD_CD = 2;
	public static final int CMD_CAT = 3;
	public static final int CMD_CURRENT = 4;
	public static final int CMD_DATE = 5;
	public static final int CMD_COEFFS = 6;
	public static final int CMD_PING = 7;
	public static final int CMD_DATE_SYNC = 8;
	public static final int CMD_COEFFS_SET = 9;

	public static final int CMD_CONNECT = 0xf0;
	public static final int CMD_DISCONNECT = 0xf1;

	public static final UUID serialPortUUID = 
			UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private static Context appContext;
	private static ScheduledExecutorService service = null;
	private static BluetoothDevice lastDev = null;
	private static BluetoothSocket socket = null;
	private static ScreenReceiver mReceiver = null;
	private static Timer delaidDisconnectTimer = null;

	private static Map<Integer, Boolean> activityStatus = 
			new HashMap<Integer, Boolean>();

	@Override
	public void onCreate() {
		super.onCreate();
		appContext = getApplicationContext();

	}

	public static Context getAppContext() {
		return appContext;
	}

	public static void SetActiveBluetoothDevice(BluetoothDevice dev) {
		try {
			if (service != null)
			{
				service.shutdownNow();
				service = null;
			}
			if (socket != null && socket.isConnected())
			{
				socket.close();
				socket = null;
			}
			//socket = dev.createRfcommSocketToServiceRecord(serialPortUUID);
			socket = dev.createInsecureRfcommSocketToServiceRecord(serialPortUUID);
			lastDev = dev;
			service = Executors.newScheduledThreadPool(1);
		} catch (IOException e) {
			lastDev = null;
		}
	}

	public static BluetoothDevice getBtDevice() {
		return lastDev;
	}

	public static boolean isBtConnected() {
		return (socket != null) && (socket.isConnected());
	}

	public static Command buildCommand(int cmdcode, myActivtyInterface ressiver,
			Object arg) 
					throws IllegalArgumentException {
		Command cmd;

		Handler h = (ressiver == null) ? null : ressiver.getHandler();
		
		switch (cmdcode) {
		case CMD_CONNECT:
			cmd = new ConnectCommand(h, ressiver, socket);
			break;
		case CMD_DISCONNECT:
			cmd = new DisconnectCommand(h, ressiver, socket);
			break;
		case CMD_CURRENT:
			cmd = new CurrentCommand(h, ressiver, socket);
			break;
		case CMD_DATE:
			cmd = new DateCommand(h, ressiver, socket);
			break;
		case CMD_DATE_SYNC:
			cmd = new SyncDateCommand(h, ressiver, socket);
			break;
		case CMD_COEFFS:
			cmd = new GetCoeffsCommand(h, ressiver, socket);
			break;
		case CMD_COEFFS_SET:
			if (arg instanceof List<?>)
				cmd = new WriteCoeffsCommand(h, ressiver, socket, (List<Coefficient>) arg);
			else
				throw new IllegalArgumentException("Arg mast be instance of List<Coefficient>");
			break;
		case CMD_PING:
			cmd = new PingCommand(h, ressiver, socket);
			break;
		case CMD_LS:
			if (arg instanceof String)
				cmd = new lsCommand(h, ressiver, socket, (String) arg);
			else
				throw new IllegalArgumentException("Arg mast be instance of String");
			break;
		case CMD_CD:
			if (arg instanceof String)
				cmd = new cdCommand(h, ressiver, socket, (String) arg);
			else
				throw new IllegalArgumentException("Arg mast be instance of String");
			break;
		case CMD_CAT:
			if (arg instanceof String)
				cmd = new catCommand(h, ressiver, socket, (String) arg);
			else
				throw new IllegalArgumentException("Arg mast be instance of String");
			break;
		default:
			throw new IllegalArgumentException("Unknown cmd code");
		}

		Log.d(LOG_TAG, "Command " + cmd.command() + " created");

		return cmd;
	}

	public static void RecreateBtSocket()
	{
		try {
			if (socket != null && socket.isConnected())
				socket.close();
			//socket = lastDev.createRfcommSocketToServiceRecord(serialPortUUID);
			socket = lastDev.createInsecureRfcommSocketToServiceRecord(serialPortUUID);
		} catch (IOException e) {
			socket = null;
		}
	}

	public static void execCommand(Command cmd) {
		if (service != null)
			service.submit(cmd);
	}

	public static ScheduledFuture<?> scheduleAtFixedRate(Command cmd, long delay) {
		if (service != null)
			return service.scheduleAtFixedRate(cmd, delay, delay, TimeUnit.MILLISECONDS);
		else
			return null;
	}

	private static void disconnect(final myActivtyInterface a)
	{
		if (delaidDisconnectTimer == null)
		{
			delaidDisconnectTimer = new Timer();
			delaidDisconnectTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					Log.d(LOG_TAG, "closeingConnection");
					execCommand(buildCommand(
							CMD_DISCONNECT, a, null));
					if (mReceiver != null)
					{
						appContext.unregisterReceiver(mReceiver);
						mReceiver = null;
					}
				}
			}, 3000);
		}
	}

	public static void ActivityChangedStatus(final myActivtyInterface a, boolean newStatus) {
		activityStatus.put(a.getID(), Boolean.valueOf(newStatus)); // change status

		boolean resultStatus = false;
		for (Boolean status : activityStatus.values())
			if (status.booleanValue())
			{
				resultStatus = true;
				break;
			}

		if (resultStatus)
		{
			if (delaidDisconnectTimer != null)
			{
				delaidDisconnectTimer.cancel();
				delaidDisconnectTimer = null;
			}

			IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
			filter.addAction(Intent.ACTION_SCREEN_OFF);
			mReceiver = new ScreenReceiver();
			mReceiver.setOnScreenStatusChanged(new ScreenReceiver.ScreeenListener() {

				@Override
				public void OnScreenStatusChanged(boolean isOn) {
					if (!isOn)
						disconnect(a);
				}
			});
			appContext.registerReceiver(mReceiver, filter);
			if (!isBtConnected())
			{
				Log.d(LOG_TAG, "connecting");
				execCommand(buildCommand(CMD_CONNECT, a, null));
			}
		} else {
			disconnect(a);
		}
	}
}
