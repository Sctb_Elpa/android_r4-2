package ru.sctbelpa.r4_2.coefficient;

import java.io.InvalidClassException;
import java.text.ParseException;

import ru.sctbelpa.r4_2.R4_2App;
import android.content.res.Resources;

public class IntCoefficient implements Coefficient {

	private final String name;
	private int value;
	private final String units;
	private int addr = 0;
	
	public IntCoefficient(String name, int value, String units) {
		this.name = name;
		this.value = value;
		this.units = units;
	}
	
	public IntCoefficient(int name_str_id, int value, String units) {
		name = R4_2App.getAppContext().getResources().getString(name_str_id);
		this.units = units;
		this.value = value;
	}
	
	public IntCoefficient(int name_str_id, String value_str, String units) {
		name = R4_2App.getAppContext().getResources().getString(name_str_id);
		this.units = units;
		value = (int)Long.parseLong(value_str);
	}
	
	public IntCoefficient(int name_str_id, String value_str, int units_str_id) {
		Resources r = R4_2App.getAppContext().getResources();
		name = r.getString(name_str_id);
		units = r.getString(units_str_id);
		value = (int)Long.parseLong(value_str);
	}
	
	@Override
	public String getUnits() {
		return units;
	}

	@Override
	public int getType() {
		return TYPE_INT;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.valueOf(value);
	}

	@Override
	public void setValueFromString(String value) throws ParseException {
		try {
			this.value = Integer.parseInt(value);
		} catch (NumberFormatException e) {
			throw new ParseException(e.getMessage(), 0);
		}
	}

	@Override
	public String formatOutput() {
		return toString();
	}

	@Override
	public Object valueObj() {
		return Integer.valueOf(value);
	}

	@Override
	public void setValueObj(Object newval) throws InvalidClassException {
		if (newval instanceof Integer)
			value = ((Integer) newval).intValue();
		else 
			throw new InvalidClassException("newval must be instance of Integer");
	}
	
	@Override
	public int getAddress() {
		return addr ;
	}

	@Override
	public void setAddress(int addr) {
		this.addr = addr;
	}
	
}
