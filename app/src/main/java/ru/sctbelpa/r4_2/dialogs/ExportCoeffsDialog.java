package ru.sctbelpa.r4_2.dialogs;

import java.io.File;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class ExportCoeffsDialog extends ImportCoeffsDialog {

	private EditText input;
	
	public ExportCoeffsDialog(Context context) {
		super(context);

		input = new EditText(context);
		input.setLayoutParams(
				new ViewGroup.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT, 
						ViewGroup.LayoutParams.WRAP_CONTENT));
		topLayout.addView(input, 0);
		topLayout.removeView(backButton);
		this.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if (listener != null)
            	{
            		if (input.getText().toString().isEmpty() && selectedIndex > -1)
            		{
            			String filename = listView.getItemAtPosition(selectedIndex).toString();
            			if (!filename.endsWith(extFilter))
            				filename += "." + extFilter;
            			listener.OnSelectedFile(filename);
            			return;
            		}
            		if (!input.getText().toString().isEmpty())
            		{
            			String filename = currentPath + "/" + input.getText().toString();
            			if (!filename.endsWith(extFilter))
            				filename += "." + extFilter;
            			listener.OnSelectedFile(filename);
            			return;
            		}
            	}
            }
        });
	}

	@Override
	protected ListView createListView(Context context) {
		ListView listView = new ListView(context);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
				final ArrayAdapter<File> adapter = (FileAdapter) adapterView.getAdapter();
				File file = adapter.getItem(index);
				if (!(file instanceof FakeFile))
				{
					if (file.isDirectory()) {
						currentPath = file.getPath();
						RebuildFiles(adapter);
					} else {
						if (index != selectedIndex)
							selectedIndex = index;
						else
							selectedIndex = -1;
						adapter.notifyDataSetChanged();
						input.setText(file.getName());
					}
				} else if (file.getName().equals(".."))
					getBack();
			}
		});
		return listView;
	}
}
