package ru.sctbelpa.r4_2.dialogs;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import ru.sctbelpa.r4_2.R;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import edu.android.openfiledialog.OpenFileDialog;

public class ImportCoeffsDialog extends OpenFileDialog {

	private final Drawable folderUpicon;
	
	protected String extFilter = null; 

	protected class FakeFile extends File {

		private final boolean isfake;
		private final String fakename;

		public FakeFile(String path) {
			super(path);
			isfake = false;
			fakename = null;
		}

		public FakeFile(boolean fake, String fakename) {
			super(".dumy");
			isfake = true;
			this.fakename = fakename;
		}

		@Override
		public String getName() {
			if (!isfake)
				return super.getName();
			else
				return fakename;
		}

	}

	protected class FileAdapterdots extends OpenFileDialog.FileAdapter {

		public FileAdapterdots(Context context, List<File> files) {
			super(context, files);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView view = (TextView) super.getView(position, convertView, parent);
			File file = getItem(position);
			if (view != null) {
				view.setText(file.getName());
				if (!(file instanceof FakeFile))
				{
					if (file.isDirectory()) {
						setDrawable(view, folderIcon);
						view.setBackgroundColor(getColor(android.R.color.transparent));
					} else {
						setDrawable(view, fileIcon);
						view.setBackgroundColor((selectedIndex == position) ?
								getColor(android.R.color.holo_blue_dark) : getColor(android.R.color.transparent));
					}
				} else if (file instanceof FakeFile){
					setDrawable(view, folderUpicon);
					view.setBackgroundColor((selectedIndex == position) ?
							getColor(android.R.color.holo_blue_dark) : getColor(android.R.color.transparent));
				}
			}
			return view;
		}

	}

	public ImportCoeffsDialog(Context context) {
		super(context);

		this.setFolderIcon(context.getResources().getDrawable(R.drawable.catalog));
		this.setFileIcon(context.getResources().getDrawable(R.drawable.data128));
		folderUpicon = context.getResources().getDrawable(R.drawable.catalogup);	
		topLayout.removeView(backButton);
	}

	@Override
	protected List<File> getFiles(String directoryPath) {
		List<File> fileList = new LinkedList<File>(super.getFiles(directoryPath));
		fileList.add(0, (File)(new FakeFile(true, "..")));
		return fileList;
	}

	@Override
	public AlertDialog show() {
		AlertDialog res = super.show();
		listView.setAdapter(new FileAdapterdots(this.context, files));
		return res;
	}

	@Override
	protected ListView createListView(Context context) {
		ListView listView = new ListView(context);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
				final ArrayAdapter<File> adapter = (FileAdapter) adapterView.getAdapter();
				File file = adapter.getItem(index);
				if (!(file instanceof FakeFile))
				{
					if (file.isDirectory()) {
						currentPath = file.getPath();
						RebuildFiles(adapter);
					} else {
						if (index != selectedIndex)
							selectedIndex = index;
						else
							selectedIndex = -1;
						adapter.notifyDataSetChanged();
					}
				} else if (file.getName().equals(".."))
					getBack();
			}
		});
		return listView;
	}

	protected void getBack()
	{
		File file1 = new File(currentPath);
		File parentDirectory = file1.getParentFile();
		if (parentDirectory != null) {
			currentPath = parentDirectory.getPath();
			RebuildFiles(((FileAdapter) listView.getAdapter()));
		}
	}

	public OpenFileDialog setExtFilter(String filter) {
		extFilter = filter;
		return setFilter(".*\\." + filter);
	}
}

