package ru.sctbelpa.r4_2;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.text.ParseException;

import ru.sctbelpa.r4_2.adepters.BtListAdepter;
import ru.sctbelpa.r4_2.adepters.CoeffsListAdepter;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import ru.sctbelpa.r4_2.commands.DateCommand;
import ru.sctbelpa.r4_2.dialogs.ExportCoeffsDialog;
import ru.sctbelpa.r4_2.dialogs.ImportCoeffsDialog;
import ru.sctbelpa.r4_2.halpers.CoefficientFileInterface;
import ru.sctbelpa.r4_2.halpers.CoefficientsXMLFile;
import ru.sctbelpa.r4_2.halpers.myActivtyInterface;
import edu.android.openfiledialog.OpenFileDialog;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements myActivtyInterface {

	public static final int ACTIVITY_ID = 0;
	
	public static final String COEFF_FILE_EXT = "DB0D";
	
	private Button selectDeviceButton;
	private BluetoothAdapter adapter;
	private List<BluetoothDevice> paredDevices;
	private AlertDialog DevSelectDialog;
	private LinearLayout progressFrame, deviceDataFrame;

	private ScheduledFuture<?> valsUpdateFuture = null;
	private ScheduledFuture<?> dateUpdateFuture = null;

	public static final int CLOSE_ALL_FRAMES = 0;
	public static final int SHOW_PROGRESS_FRAME = 1;
	public static final int SHOW_DATA_FRAME = 2;

	private TextView mesureLabels[];
	private TextView devDateLabel;
	private Handler h;

	private Date devDate = null;

	private View.OnClickListener coeffsBtnListener = new View.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			R4_2App.execCommand(R4_2App.buildCommand(
					R4_2App.CMD_COEFFS, MainActivity.this, null));
		}
	};

	private View.OnClickListener historyBtnListener = new View.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Intent intent = new Intent(MainActivity.this, FileListActivity.class);
			startActivity(intent);
		}
	};

	private View.OnClickListener syncClockListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			if (devDate != null)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle(R.string.Sync_clock_str);
				Date current_date = new Date();
				Date _devDate = (Date)devDate.clone();

				long delta_s = (_devDate.getTime() - current_date.getTime());
				delta_s /= 1000; // sec
				builder.setMessage(getResources().getString(R.string.Sync_cloc_desc) + " " +
						delta_s + " " +
						getResources().getString(R.string.sec) + "\n" +
						getResources().getString(R.string.Sync_clock_str));
				builder.setCancelable(true);
				builder.setNegativeButton(android.R.string.no, null);
				builder.setPositiveButton(android.R.string.yes, 
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dlg, int arg1) {
						R4_2App.execCommand(R4_2App.buildCommand(
								R4_2App.CMD_DATE_SYNC, MainActivity.this, null));
					}
				});
				builder.create().show();
			}
		}
	};

	private View.OnClickListener selectdeviceOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setTitle(R.string.pairedDevisesList);
			ListView list = new ListView(MainActivity.this);
			list.setAdapter(new BtListAdepter(MainActivity.this, paredDevices));
			list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (DevSelectDialog.isShowing())
						DevSelectDialog.dismiss();

					BluetoothDevice dev = paredDevices.get(position);
					R4_2App.SetActiveBluetoothDevice(dev);
					RefrashStatus();
					TryConnect();
				}
			});
			builder.setView(list);
			DevSelectDialog = builder.create();
			DevSelectDialog.show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		paredDevices = new LinkedList<BluetoothDevice>();
		adapter =  BluetoothAdapter.getDefaultAdapter();

		selectDeviceButton = (Button)findViewById(R.id.DevSelectBtn);
		selectDeviceButton.setOnClickListener(selectdeviceOnClickListener);

		deviceDataFrame = (LinearLayout)findViewById(R.id.device_data_layout);
		progressFrame = (LinearLayout)findViewById(R.id.ConnectingProgressFrame);

		h = new Handler();

		switchView(CLOSE_ALL_FRAMES);

		mesureLabels = new TextView[4];
		mesureLabels[0] = (TextView)findViewById(R.id.PressureLabel);
		mesureLabels[1] = (TextView)findViewById(R.id.TemperatureLabel);
		mesureLabels[2] = (TextView)findViewById(R.id.PressureFreqLabel);
		mesureLabels[3] = (TextView)findViewById(R.id.TemperatureFreqLabel);

		devDateLabel = (TextView)findViewById(R.id.DevClockLabel);
		devDateLabel.setOnClickListener(syncClockListener);

		((Button)findViewById(R.id.BTN_coeffs)).setOnClickListener(coeffsBtnListener);
		((Button)findViewById(R.id.BTN_history)).setOnClickListener(historyBtnListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	protected void onResume() {
		super.onResume();
		if (adapter == null)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
					new ContextThemeWrapper(MainActivity.this, R.style.popup_theme));
			builder.setTitle(R.string.E_bluetooth_not_supported)
			.setMessage(R.string.E_bluetooth_not_supported_)
			.setCancelable(false)
			.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			})
			.create().show();
			return;
		}
		else
		{
			if (!adapter.isEnabled())
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(
						new ContextThemeWrapper(MainActivity.this, R.style.popup_theme));
				builder.setTitle(R.string.bt_request)
				.setMessage(R.string.bt_request_desc)
				.setCancelable(false)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Intent enBTintent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
						startActivity(enBTintent);
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});	
				builder.create().show();
			}
			else
			{
				if (adapter.isDiscovering())
					adapter.cancelDiscovery();

				paredDevices.clear();
				for (BluetoothDevice dev : adapter.getBondedDevices())
					paredDevices.add(dev);

				TryConnect();
			}
		}
		Log.d("MainActivity", "OnResume()");
	};

	@Override
	protected void onPause() {
		enableUpdateStatus(false);
		R4_2App.ActivityChangedStatus(this, false);
		super.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void TryConnect()
	{
		R4_2App.ActivityChangedStatus(this, true);
		if (R4_2App.getBtDevice() != null)
		{
			if (!R4_2App.isBtConnected())
				switchView(SHOW_PROGRESS_FRAME);
			else
			{
				enableUpdateStatus(true);
				switchView(SHOW_DATA_FRAME);
			}
		}
	}

	private void enableUpdateStatus(boolean enable)
	{
		if (enable)
		{
			// add cyclicly call "current"
			if (valsUpdateFuture == null)
				valsUpdateFuture = R4_2App.scheduleAtFixedRate(
						R4_2App.buildCommand(R4_2App.CMD_CURRENT, this, null), 500);

			// add cyclicly call "date"
			if (dateUpdateFuture == null)
				dateUpdateFuture = R4_2App.scheduleAtFixedRate(
						R4_2App.buildCommand(R4_2App.CMD_DATE, this, null), 1000);
		}
		else
		{
			// disable refresh vals
			if (valsUpdateFuture != null && !valsUpdateFuture.isCancelled())
			{
				valsUpdateFuture.cancel(false);
				valsUpdateFuture = null;
			}
			// disable refresh date
			if (dateUpdateFuture != null && !dateUpdateFuture.isCancelled())
			{
				dateUpdateFuture.cancel(false);
				dateUpdateFuture = null;
			}
		}
	}

	public void RefrashStatus()
	{
		if (R4_2App.getBtDevice() != null && R4_2App.isBtConnected())
		{
			switchView(SHOW_DATA_FRAME);
			enableUpdateStatus(true);
		}
		else
		{
			switchView(CLOSE_ALL_FRAMES);
			enableUpdateStatus(false);
		}
	}

	public void switchView(int state)
	{
		switch (state)
		{
		case CLOSE_ALL_FRAMES:
			progressFrame.setVisibility(View.INVISIBLE);
			deviceDataFrame.setVisibility(View.INVISIBLE);
			selectDeviceButton.setText(R.string.select_device);
			return;
		case SHOW_DATA_FRAME:
			progressFrame.setVisibility(View.INVISIBLE);
			deviceDataFrame.setVisibility(View.VISIBLE);
			break;
		case SHOW_PROGRESS_FRAME:
			progressFrame.setVisibility(View.VISIBLE);
			deviceDataFrame.setVisibility(View.INVISIBLE);
			break;
		}
		BluetoothDevice d = R4_2App.getBtDevice();
		if (d != null)
			selectDeviceButton.setText(d.getName());
	}

	public void updateValues(float values[]) {
		if (values.length != 4)
			return;
		else 
			for (int i = 0; i < 4; ++i)
				mesureLabels[i].setText(String.valueOf(values[i]));
	}

	public void updateDeviceClockDisplay(Date devClockValue) {
		devDate = devClockValue;
		SimpleDateFormat formatter = new SimpleDateFormat(DateCommand.dateFormat);
		devDateLabel.setText(formatter.format(devClockValue));
	}

	public void showCoeffsEditDialog(List<Coefficient> coeffs)
	{
		final List<Coefficient> _coeffs = coeffs; // save for edit

		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		builder.setTitle(R.string.CoeffsListname);

		View _list =  getLayoutInflater().inflate(R.layout.coeffs_list_layout, null); 

		// list
		ListView list = (ListView)_list.findViewById(R.id.coeffs_list);
		final CoeffsListAdepter adepter = new CoeffsListAdepter(MainActivity.this, _coeffs);
		list.setAdapter(adepter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {

				final int _position = position;
				AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
				builder2.setTitle(getResources().getString(R.string.change_coeff) + " " +
						_coeffs.get(position).getName());
				//builder2.setIcon(adepter.getitemImageRes(position));
				final EditText input = new EditText(MainActivity.this);
				input.setText(_coeffs.get(_position).toString());
				input.selectAll();
				builder2.setView(input);
				builder2.setCancelable(true);

				// disable close dialog on "Ok" button
				builder2.setPositiveButton(android.R.string.ok, null);
				final AlertDialog d = builder2.create();
				d.setOnShowListener(new DialogInterface.OnShowListener() {

					@Override
					public void onShow(DialogInterface dialog) {
						Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);

						b.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View view) {
								try {
									_coeffs.get(_position).setValueFromString(input.getText().toString());
									adepter.notifyDataSetChanged();
									d.dismiss();
								} catch (ParseException e) {
									input.setText(R.string.incorrect_coeff_value);
									input.selectAll();
								}	
							}
						});
					}
				});
				d.show();
			}
		});

		builder.setView(_list);

		builder.setCancelable(true);
		builder.setNegativeButton(android.R.string.cancel, null);
		builder.setPositiveButton(android.R.string.ok, 
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dlg, int arg1) {
				R4_2App.execCommand(R4_2App.buildCommand(
						R4_2App.CMD_COEFFS_SET, MainActivity.this, _coeffs));
			}
		});

		// import
		ImageButton importBtn = (ImageButton)_list.findViewById(R.id.import_coeffs_btn);
		importBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ImportCoeffsDialog fileDialog = new ImportCoeffsDialog(MainActivity.this);
				fileDialog.setExtFilter(COEFF_FILE_EXT);
				fileDialog.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {

					@Override
					public void OnSelectedFile(String fileName) {
						CoefficientFileInterface reader = new CoefficientsXMLFile(fileName, _coeffs);
						try {
							reader.Import();
							adepter.notifyDataSetChanged();
						} catch (IOException e) {
							Toast.makeText(MainActivity.this, R.string.read_coeffs_file_failed,
									Toast.LENGTH_SHORT).show();
						} catch (ParseException e) {
							Toast.makeText(MainActivity.this, e.getMessage(),
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				fileDialog.show();
			}
		});

		// export
		ImageButton exportBtn = (ImageButton)_list.findViewById(R.id.export_coeffs_btn);
		exportBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ExportCoeffsDialog fileDialog = new ExportCoeffsDialog(MainActivity.this);
				fileDialog.setExtFilter(COEFF_FILE_EXT);
				fileDialog.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {

					@Override
					public void OnSelectedFile(String fileName) {
						CoefficientFileInterface writer = new CoefficientsXMLFile(fileName, _coeffs);
						try {
							writer.Export();
							Toast.makeText(MainActivity.this, R.string.write_coeffs_file_success,
									Toast.LENGTH_SHORT).show();
						} catch (IOException e) {
							Toast.makeText(MainActivity.this, R.string.write_coeffs_file_failed,
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				fileDialog.show();
			}
		});

		builder.create().show();
	}

	@Override
	public 	Handler getHandler() {
		return h;
	}

	@Override
	public Integer getID() {
		return Integer.valueOf(ACTIVITY_ID);
	}

	@Override
	public Context getContext() {
		// TODO Auto-generated method stub
		return this;
	}
}