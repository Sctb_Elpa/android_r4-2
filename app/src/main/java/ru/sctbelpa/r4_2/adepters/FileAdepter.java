package ru.sctbelpa.r4_2.adepters;

import java.util.List;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.FsModel.FSContent;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileAdepter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	private List<FSContent.FsObject> objects;

	private static final String F_CSV = ".csv"; 

	public FileAdepter(Context c, List<FSContent.FsObject> items)
	{
		context = c;
		objects = items;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Object getItem(int pos) { 
		return objects.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View container, ViewGroup parent) {

		//TODO
		View view = container;
		if (view == null)
			view = inflater.inflate(R.layout.file_item, parent, false);

		FSContent.FsObject obj = (FSContent.FsObject)getItem(position);

		String filename = obj.name;
		if (filename.endsWith(F_CSV))
			filename = filename.replace(F_CSV, "");

		((TextView)view.findViewById(R.id.Filename)).setText(filename);
		((ImageView)view.findViewById(R.id.FsObjectIcon_icon)).setImageResource(obj.IconId());
		
		return view;
	}
}
