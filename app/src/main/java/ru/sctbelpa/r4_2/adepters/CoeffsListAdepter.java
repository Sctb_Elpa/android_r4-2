package ru.sctbelpa.r4_2.adepters;

import java.util.List;

import ru.sctbelpa.r4_2.R;
import ru.sctbelpa.r4_2.coefficient.Coefficient;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CoeffsListAdepter extends BaseAdapter {

	private final List<Coefficient> coeffs;
	private final LayoutInflater inflater;

	public CoeffsListAdepter(Context context, List<Coefficient> coeffs) {
		this.coeffs = coeffs;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	} 

	@Override
	public int getCount() {
		return coeffs.size();
	}

	@Override
	public Object getItem(int item) {
		return coeffs.get(item);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	public int getitemImageRes(int position) {
		Coefficient c = coeffs.get(position);
		switch (c.getType())
		{
		case Coefficient.TYPE_FLOAT:
			return R.drawable.f;
		case Coefficient.TYPE_INT:
			return R.drawable.i;
		default: return 0;
		}
	}

	@Override
	public View getView(int position, View container, ViewGroup parent) {

		View view = container;
		if (view == null)
			view = inflater.inflate(R.layout.coeffs_list_item, parent, false);

		Coefficient c = coeffs.get(position);

		ImageView icon = (ImageView)view.findViewById(R.id.coeff_icon);
		icon.setImageResource(getitemImageRes(position));
				
		((TextView)view.findViewById(R.id.coeff_name)).setText(c.getName());
		((TextView)view.findViewById(R.id.coeff_value)).setText(c.toString());
		((TextView)view.findViewById(R.id.coeff_units)).setText(c.getUnits());

		return view;

	}

}
